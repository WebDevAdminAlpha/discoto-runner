## Missing Project/Group

Discoto does not seem to be monitoring discussions at:

<!-- FILL THIS OUT -->
```
path/to/group/or/project
```

Please add this group/project to the scheduled pipelines

/label ~"devops::secure" ~"group::vulnerability research"
/assign @d0c-s4vage
